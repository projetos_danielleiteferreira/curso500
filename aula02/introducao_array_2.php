<?php

$aluno1 = [
    'Nome'      => 'Patricia Costa',
    'Curso'     => [
        'Nome' => 'Analise de Sistema',
        'Disciplinas' => [ 
            'Logica de Programacao',
            'Introducao a computacao',
            'Orientacao a Objetos'
        ]
    ],
    'Periodo'   => 'Noturno'
];

$aluno2 = [
    'Nome'      => 'Daniel Ferreira',
    'Curso'     => 'Desenvolvimento',
    'Periodo'   => 'Manhã'
];

$aluno3 = [
    'Nome'      => 'Denise Santos',
    'Curso'     => 'Pedagogia',
    'Periodo'   => 'Tarde'
];

echo '<pre>';
/*
 * A função print_r é usada para imprimir
 * a estrutura de um array de uma forma mais limpa
 * */

print_r($aluno1);

echo '<hr>';



$alunos = [$aluno1, $aluno2, $aluno3];

echo "<pre>";

print_r($alunos);

echo '<hr>';

echo 'Nome: ' . $alunos[0]['Nome'];
echo '<br>Curso: ' . $alunos[0]['Curso']['Nome'];
echo '<br>Disciplinas: ' . $alunos[0]['Curso']['Disciplinas'][1];
echo '<br>Período: ' . $aluno1['Periodo'];

echo "<hr>";


echo '<hr>';



