<?php

//Toda variável no PHP inicia com $

$nome = "Aline dos Santos";
$email = "aline.santos@gmail.com";
$salario = 5.005;

echo $nome;
echo "<br>";
echo $email;
echo "<br>";
echo $salario;

// a função var_dump é utilizada para imprimir o valor e tipo da variável
echo "<hr>";
var_dump($nome);
echo "<br>";
var_dump($email);
echo "<br>";
var_dump($salario);

echo "<hr>";

//Converter os tipos das variáveis

$salario = (int)$salario;
var_dump($salario);

echo "<br>";

$nome = (int)$nome;
var_dump($nome);

echo "<br>";


var_dump($email);
