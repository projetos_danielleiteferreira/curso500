<?php

$nomeCompleto = 'Patricia Costa'; //Escopo Global

function nome($nome, $sobrenome)
{
    $nomeCompleto = "$nome $sobrenome"; //Escopo Local
    echo $nomeCompleto;
   
}

echo $nomeCompleto; //Variavel escopo Global

echo '<br>';

nome('Daniel', 'Ferreira');

$email = 'patricia.costa@gmail.com';

function relatorio()
{
    global $nomeCompleto;
    global $email;
    echo "Nome: $nomeCompleto<br>";
    echo "E-mail $email";
}

echo "<hr>";

relatorio();

echo "<hr>";


function verNome(&$nome) //&$nome é variável de referencia que só recebe de retorno uma variavel
{
    echo "Antes de Alterar: $nome<br>";
    $nome = 'Nome Alterado';
    echo "Depois de Alterar: $nome<br>";
}

$ref = 'Daniel Ferreira';

verNome($ref);

echo $ref;


echo "<hr>";

$string = 'PHP, Phython, NodeJS, Ruby';

$array = explode(',', $string); //converte uma string para array

echo '<hr><pre>';
print_r($array);

echo '<br>';

$string = 'Substituir qualquer coisa dentro da string';
$novaString = str_replace('qualquer coisa', 'uma string qualquer', $string);

echo $novaString;