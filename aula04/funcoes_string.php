<?php

//funções para manipulação de string

//procura valor na string

$email = 'tesTE@gmail.com';
$retorno = strstr($email, '@');
echo $retorno;

echo "<br>";

$retorno = strstr($email, '@', TRUE);
echo $retorno;

// retorna posicao da string @
echo '<br>' . strpos($email, '@');

echo "<hr>";

echo substr($email, 5);

echo "<br>";

//faz a leitura de um ponto a outro ponto
echo substr($email, 0, 9);

echo "<hr>";

//conta os caracteres da varial
echo strlen($email);

echo "<hr>"; 

//converte tudo para maiúscula 
echo strtoupper($email) . '<br>';

echo strtolower("CONVERTE TUDO PARA MINÚSCULO");

echo "<hr>";

//Maiuscula primeira letra do nome
echo ucfirst('maria');

echo "<hr>";

//Conta a quantidade de palavras 
echo str_word_count('Vamos ver se ele conta quantas palavras tem neste string');

echo "<br>";

//remove espaços em branco d string
echo trim('   teste     ');


