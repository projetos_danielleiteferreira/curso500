--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.1 (Ubuntu 11.1-3.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alunos; Type: TABLE; Schema: public; Owner: aplicacao
--

CREATE TABLE public.alunos (
    id integer NOT NULL,
    nome character varying NOT NULL,
    serie character varying NOT NULL,
    turma character varying NOT NULL,
    curso character varying DEFAULT 'PHP'::character varying NOT NULL
);


ALTER TABLE public.alunos OWNER TO aplicacao;

--
-- Name: alunos_id_seq; Type: SEQUENCE; Schema: public; Owner: aplicacao
--

CREATE SEQUENCE public.alunos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alunos_id_seq OWNER TO aplicacao;

--
-- Name: alunos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aplicacao
--

ALTER SEQUENCE public.alunos_id_seq OWNED BY public.alunos.id;


--
-- Name: cursos; Type: TABLE; Schema: public; Owner: aplicacao
--

CREATE TABLE public.cursos (
    id integer NOT NULL,
    nome character varying NOT NULL
);


ALTER TABLE public.cursos OWNER TO aplicacao;

--
-- Name: cursos_id_seq; Type: SEQUENCE; Schema: public; Owner: aplicacao
--

CREATE SEQUENCE public.cursos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cursos_id_seq OWNER TO aplicacao;

--
-- Name: cursos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aplicacao
--

ALTER SEQUENCE public.cursos_id_seq OWNED BY public.cursos.id;


--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: aplicacao
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    email character varying NOT NULL,
    senha character varying NOT NULL,
    nome character varying NOT NULL,
    perfil integer DEFAULT 2
);


ALTER TABLE public.usuarios OWNER TO aplicacao;

--
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: aplicacao
--

CREATE SEQUENCE public.usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO aplicacao;

--
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aplicacao
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- Name: alunos id; Type: DEFAULT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.alunos ALTER COLUMN id SET DEFAULT nextval('public.alunos_id_seq'::regclass);


--
-- Name: cursos id; Type: DEFAULT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.cursos ALTER COLUMN id SET DEFAULT nextval('public.cursos_id_seq'::regclass);


--
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- Data for Name: alunos; Type: TABLE DATA; Schema: public; Owner: aplicacao
--

COPY public.alunos (id, nome, serie, turma, curso) FROM stdin;
3	Daniel Leite Ferreira	12	Noite	PHP
2	Jose Wilson Ferreira	5	Noite	PHP
5	Lililian 	516	1651	PHP
6	ROGERIO SKY	15	TARDE	JAVASCRIPT
\.


--
-- Data for Name: cursos; Type: TABLE DATA; Schema: public; Owner: aplicacao
--

COPY public.cursos (id, nome) FROM stdin;
1	PHP
2	PYTHON
3	JAVASCRIPT
\.


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: aplicacao
--

COPY public.usuarios (id, email, senha, nome, perfil) FROM stdin;
48	selma@uol.com.br	123456	Selma Dias Santos	1
3	daniel@uol.com	123456	Daniel Leite Ferreira	2
68	daniel.issomesmo@gmail.com	159	Andreza Nascimento	1
1	admin@apo.com	15	Administrador do Sistema	1
79	daniel.issomesmo@gmail.com.kk	dd	Andreza Nascimento	1
84	1@1.com	dsfadsf	Andreza Nascimento	1
\.


--
-- Name: alunos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aplicacao
--

SELECT pg_catalog.setval('public.alunos_id_seq', 8, true);


--
-- Name: cursos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aplicacao
--

SELECT pg_catalog.setval('public.cursos_id_seq', 3, true);


--
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aplicacao
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 84, true);


--
-- Name: alunos alunos_pkey; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.alunos
    ADD CONSTRAINT alunos_pkey PRIMARY KEY (id);


--
-- Name: cursos cursos_pkey; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.cursos
    ADD CONSTRAINT cursos_pkey PRIMARY KEY (id);


--
-- Name: usuarios usuarios_email_key; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_email_key UNIQUE (email);


--
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

