<?php

$array = [
    'Ana Paula', 
    'Patricia Lima',
    'Isabela Oliveira',
    'Carolina Dias'
];
//ordena por valor   
sort($array);

echo "<pre>";
print_r($array);

echo "<hr>";

$array2 = [
    10 => 'Ana Paula',
    7  => 'Patricia Lima',
    3  => 'Isabela Oliveira',
    1  => 'Carolina Dias'
];

//ksort ordena pela chave
ksort($array2);
echo '<pre>';
print_r($array2);


echo '<hr>';

//checar se a chave do array existe
var_dump(array_key_exists('nome', ['nome' => 'Maria']));

echo '<hr>';

var_dump(in_array('Maria', [
    'nome' => 'Maria'
]));

echo "<hr>";

var_dump(array_search('Maria', [
    'nome' => 'Maria'
    
]));

echo '<hr>';

echo '<pre>';

//retorna somente os valores
print_r(array_values($array));

echo '<hr>';

//retornar somente as chaves
print_r(array_keys(['nome' => 'Maria', 'email' => 'm@ig.com.br']));

echo '<hr>';

$array3 = [
    'a@gmail.com',
    'b@gmail.com',
    'a@gmail.com',
    'b@gmail.com'
];

echo '<br>';
print_r(array_unique($array3));

echo '<br>';

print_r(array_merge($array, $array2, $array3));

echo '<br>';

echo implode(' - ', $array2);


echo '<hr>';

//filtrando itens do array com array_filter

$array4 = [
    'a@gmail.com',
    'b@gmail.com',
    'dfldlf.com.br',
    'dfjdfld.ig.com.br',
    'a@gmail.com',
    'b@gmail.com'
];

echo '<hr>';

$arrayFiltrado = array_filter($array4, function ($email) {
    return strstr($email, 'ig');
});

print_r($arrayFiltrado);

echo '<hr>';

//retornar o tamanho de um array

echo count($array);