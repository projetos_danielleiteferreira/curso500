<?php require 'verifica_login.php';?>
<html>
    <head>
    	<title>Alteração de Usuários</title>
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
		<?php include 'menu.php'; ?>
		<?php require 'verifica_perfil.php';?>
        <di id="main">
        	<?php 
        	   //Inicio do Fluxo
        	   require 'conexao.php';
        	  
        	   $id = ($_GET['id']) ?? null;
        	   $query = "select * from usuarios where id=$id";
        	   $result = pg_query($query);
        	   $usuario = pg_fetch_assoc($result);
        	   
        	   if($_POST){
        	       if(empty($_POST['nome'])){
        	           $errorNome = '
                        <div class="alert alert-danger" role="alert">
                          Nome é campo obrigatório!
                        </div>';
                            	    
        	       }
        	       
        	       if(empty($_POST['email'])){
        	           $errorEmail = '
                        <div class="alert alert-danger" role="alert">
                          E-mail é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       if(empty($_POST['senha'])){
        	           $errorSenha = '
                        <div class="alert alert-danger" role="alert">
                          Senha é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       if(empty($_POST['perfil'])){
        	           $errorPerfil = '
                        <div class="alert alert-danger" role="alert">
                          Senha é campo obrigatório!
                        </div>';
        	       }
        	       
        	       $nome   = $_POST['nome'];
        	       $email  = $_POST['email'];
        	       $senha  = $_POST['senha'];
        	       $perfil = $_POST['perfil'];
        	       $query  = "update usuarios set
                                nome='$nome',
                                email='$email',
                                senha='$senha',
                                perfil='$perfil'
                                    where id=$id";
        	       $result = false;
       	       
 	       
        	       if (! isset($errorNome) && 
        	           ! isset($errorEmail) &&
        	           ! isset($errorSenha) && 
        	           ! isset($errorPerfil)){
        	           
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   header('location:listar_usuarios.php');
        	                   
        	               }else{
        	                   echo '<div class="row col-sm-10 alert-danger">
                                <h5> Erro ao salvar os dados!<h5>
        	                 </div>';
        	               }
        	       }
  
        	   }
        	?>
        	
        	
        
        	<form action="" method="post">
        			<div class="form-group row">
                  	<div class="col-sm-5">
                      <h2>+ Alterar usuário</h2>
                 </div>
                  	                
                 </div>
        	
                  <div class="form-group row">

                    <label for="inputNome" class="col-sm-2 col-form-label">Nome</label>
                    <div class="col-sm-5">
                      <input type="text" name="nome" value="<?= isset($usuario['nome']) ? $usuario['nome'] : '' ?>" class="form-control" id="inputNome" placeholder="Nome">
                      	<?= isset($errorNome) ? $errorNome : '' ?>
                 </div>
                   </div>
                     <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-5">
                      <input type="email" name="email" value="<?= isset($usuario['email']) ? $usuario['email'] : '' ?>" class="form-control" id="inputEmail3" placeholder="Email">
                      <?= isset($errorEmail) ? $errorEmail : ''?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Senha</label>
                    <div class="col-sm-5">
                      <input type="password" name="senha" value="<?= isset($usuario['senha']) ? $usuario['senha'] : '' ?>" class="form-control" id="inputPassword3" placeholder="Senha">
                      <?= isset($errorSenha) ? $errorSenha : '' ?>
                    </div>
                  </div>
            	  <div class="form-group row">
                    <label for="inputPerfil" class="col-sm-2 col-form-label">Perfil do Usuário</label>
                    <div class="col-sm-5">
                      <select class="custom-select custom-select-lg mb-3" name="perfil">
                      <option value="">Perfil de Usuário</option>
                      <option value="1" <?= $usuario['perfil'] == 1 ? ' selected' : '' ?>> Administrador</option>
                      <option value="2" <?= $usuario['perfil'] == 2 ? ' selected' : '' ?>> Secretaria</option>
                  </select>
                      <?= isset($errorPerfil) ? $errorPerfil : '' ?>
                    </div>
                  </div>      
            
                  <div class="form-group row">
                    <div class="col-sm-1" style="margin-left:230px">
                      <button type="submit" class="btn btn-primary">ALTERAR</button>
                    </div>
                  </div>
                </form>
        </div>
       </body>
</html>       