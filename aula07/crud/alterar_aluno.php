<?php require 'verifica_login.php';?>
<html>
    <head>
    	<title>Alteração de Alunos</title>
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
		<?php include 'menu.php'; ?>

        <di id="main">
        	<?php 
        	   //Inicio do Fluxo
        	   require 'conexao.php';
        	  
        	   $id = ($_GET['id']) ?? null;
        	   $query = "select * from alunos where id=$id";
        	   $result = pg_query($query);
        	   $usuario = pg_fetch_assoc($result);
        	   
        	   if($_POST){
        	       if(empty($_POST['nome'])){
        	           $errorNome = '
                        <div class="alert alert-danger" role="alert">
                          Nome é campo obrigatório!
                        </div>';
                            	    
        	       }
        	       
        	       if(empty($_POST['serie'])){
        	           $errorSerie = '
                        <div class="alert alert-danger" role="alert">
                          Série é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       if(empty($_POST['turma'])){
        	           $errorTurma = '
                        <div class="alert alert-danger" role="alert">
                          Turma é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       $nome   = $_POST['nome'];
        	       $serie  = $_POST['serie'];
        	       $turma  = $_POST['turma'];
        	       $query  = "update alunos set
                                nome='$nome',
                                serie='$serie',
                                turma='$turma'
                                    where id=$id";
        	       $result = false;
       	       
 	       
        	       if (! isset($errorNome) && 
        	       ! isset($errorSerie) &&
        	           ! isset($errorTurma)){
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   header('location:listar_alunos.php');
        	                   
        	               }else{
        	                   echo '<div class="row col-sm-10 alert-danger">
                                <h5> Erro ao salvar os dados!<h5>
        	                 </div>';
        	               }
        	       }
        	       
        	       
        	        
        	   }
        	?>
        
        	<form action="" method="post">
        			<div class="form-group row">
                  	<div class="col-sm-5">
                      <h2>+ Alterar alunos</h2>
                 </div>
                  	
                  	
                    
                 </div>
        	
                  <div class="form-group row">

                    <label for="inputNome" class="col-sm-2 col-form-label">Nome</label>
                    <div class="col-sm-5">
                      <input type="text" name="nome" value="<?= isset($usuario['nome']) ? $usuario['nome'] : '' ?>" class="form-control" id="inputNome" placeholder="Nome">
                      	<?= isset($errorNome) ? $errorNome : '' ?>
                 </div>
                   </div>
                  <div class="form-group row">
                    <label for="inputSerie" class="col-sm-2 col-form-label">Série</label>
                    <div class="col-sm-5">
                      <input type="serie" name="serie" value="<?= isset($usuario['serie']) ? $usuario['serie'] : '' ?>" class="form-control" id="inputSerie" placeholder="Série">
                      <?= isset($errorSerie) ? $errorSerie : ''?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputTurma" class="col-sm-2 col-form-label">Turma</label>
                    <div class="col-sm-5">
                      <input type="turma" name="turma" value="<?= isset($usuario['turma']) ? $usuario['turma'] : '' ?>" class="form-control" id="inputTurma" placeholder="Turma">
                      <?= isset($errorTurma) ? $errorTurma : ''?>
                    </div>
                  </div>
 
                  
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-1" style="margin-left:230px">
                      <button type="submit" class="btn btn-primary">ALTERAR</button>
                    </div>
                  </div>
                </form>
        </div>
       </body>
</html>       