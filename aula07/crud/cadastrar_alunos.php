<?php require 'verifica_login.php';?>
<html>
    <head>
    	<title>Listagem de Usuários</title>
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
		<?php include 'menu.php'; ?>
		
        <di id="main">
        	<?php 
        	   //Inicio do Fluxo
        	   require 'conexao.php';
        	   if($_POST){
        	       if(empty($_POST['nome'])){
        	           $errorNome = '
                        <div class="alert alert-danger" role="alert">
                          Nome é campo obrigatório!
                        </div>';
                            	    
        	       }
        	       
        	       if(empty($_POST['serie'])){
        	           $errorSerie = '
                        <div class="alert alert-danger" role="alert">
                          Série é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       if(empty($_POST['turma'])){
        	           $errorTurma = '
                        <div class="alert alert-danger" role="alert">
                          Turma é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       if(empty($_POST['curso'])){
        	           $errorCurso = '
                        <div class="alert alert-danger" role="alert">
                          Turma é campo obrigatório!
                        </div>';
        	           
        	       }
        	       
        	       
        	       $nome   = $_POST['nome'];
        	       $serie  = $_POST['serie'];
        	       $turma  = $_POST['turma'];
        	       $curso  = $_POST['curso']; 
        	       $query  = "insert into alunos
                                (nome,serie,turma,curso) 
                                values('$nome','$serie','$turma', '$curso')";
        	       
        	       $result = false;
       	       
 	       
        	       if (! isset($errorNome) && 
        	       ! isset($errorSerie) &&
        	           ! isset($errorTurma) &&
        	           ! isset($errorCurso)){
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   header('location:listar_alunos.php');
        	                   
        	               }else{
        	                   echo '<div class="row col-sm-10 alert-danger">
                                <h5> Erro ao salvar os dados!<h5>
        	                 </div>';
        	               }
        	       }
        	       
        	       
        	        
        	   }
        	?>
        
        	<form action="" method="post">
        			<div class="form-group row">
                  	<div class="col-sm-5">
                      <h2>+ Inserir novo aluno</h2>
                 </div>
                  	
                  	
                    
                 </div>
        	
                  <div class="form-group row">
                    <label for="inputNome" class="col-sm-2 col-form-label">Nome</label>
                    <div class="col-sm-5">
                      <input type="text" name="nome" value="<?= isset($_POST['nome']) ? $_POST['nome'] : '' ?>" class="form-control" id="inputNome" placeholder="Nome">
                      	<?= isset($errorNome) ? $errorNome : '' ?>
                 </div>
                   </div>
                 <div class="form-group row">
                    <label for="inputSerie" class="col-sm-2 col-form-label">Série</label>
                    <div class="col-sm-5">
                      <input type="text" name="serie" value="<?= isset($_POST['serie']) ? $_POST['serie'] : '' ?>" class="form-control" id="inputSerie" placeholder="Serie">
                      	<?= isset($errorSerie) ? $errorSerie : '' ?>
                    </div>
                  </div>
                 
                 </div>
                   </div>
                 <div class="form-group row">
                    <label for="inputTurma" class="col-sm-2 col-form-label">Turma</label>
                    <div class="col-sm-5">
                      <input type="text" name="turma" value="<?= isset($_POST['turma']) ? $_POST['turma'] : '' ?>" class="form-control" id="inputTurma" placeholder="Turma">
                      	<?= isset($errorTurma) ? $errorTurma : '' ?>
                    </div>
                  </div>
                    
                    <div class="form-group row">
                    <label for="inputCurso" class="col-sm-2 col-form-label">Curso</label>
                    <div class="col-sm-5">
                      <select class="custom-select custom-select-lg mb-3" name="curso">
                          <option value="" selected>Curso</option>
                          <?php 
                          $query = "select * from cursos";
                          $result = pg_query($query);
                          $cursos = pg_fetch_all($result);
                          
                          foreach ($cursos as $curso):
                        
                          ?>
                          <option value="<?=$curso['nome']?>"><?=$curso['nome']?></option>
                        
                        	<?php endforeach;?>
                          
                        </select>
                    	<?= isset($errorCurso) ? $errorCurso : '' ?>
                    </div>
                  </div>
                    
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-5" style="margin-left:230px">
                      <button type="submit" class="btn btn-primary">CADASTRAR</button>
                    </div>
                  </div>
                </form>
        </div>
       </body>
</html>       