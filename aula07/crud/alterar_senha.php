<?php require 'verifica_login.php';?>
<html>
    <head>
    	<title>Alteração de Usuários</title>
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
		<?php include 'menu.php'; ?>
		

        <di id="main">
        	<?php 
        	   //Inicio do Fluxo
        	   require 'conexao.php';
        	   require 'utils.php';
        	           	   
        	   if($_POST){
        	       if(empty($_POST['senha'])){
        	           $errorSenha = alerta('Senha é campo obrigatório!');
        	           
        	       }
        	       
        	       if(empty($_POST['confirmaSenha'])){
        	           $errorConfirmaSenha = alerta('Confirme a senha!');
        	       }
        	       
        	       if($_POST['senha'] != $_POST['confirmaSenha']){
        	           $errorComparaSenha = alerta('Senhas não conferem');
        	       }

        	       $senha   = $_POST['senha'];
        	       
        	       $id = $_SESSION['id'];
      	       
        	       $query  = "update usuarios set
                                senha='$senha'
                                  where id=$id";
    
        	       $result = false;
        	         	       
 	       
        	       if (! isset($errorSenha) && 
        	           ! isset($errorConfirmaSenha) && 
        	           ! isset($errorComparaSenha)){
        	           
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   echo alert('Senha alterada com sucesso!', 'success');
                                            	                   
        	               }else{
        	                   echo alerta('Erro ao salvar os dados!');
        	               }
        	       }
  
        	   }
        	?>
        	
        	 <div class="form-group row">
                    
                    <div class="col-sm-5" tyle="margin-left:200px">
                      
                      <?= isset($errorComparaSenha) ? $errorComparaSenha : '' ?>
                      
                    </div>
                    </div>
        
        	<form action="" method="post">
        			<div class="form-group row">
                  	<div class="col-sm-5">
                      <h2>+ Alterar senha do usuário</h2>
                   </div>
                 </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Senha</label>
                    <div class="col-sm-5">
                      <input type="password" name="senha" value="<?= isset($usuario['confirmasenha']) ? $usuario['confirmasenha'] : '' ?>" class="form-control" id="inputPassword3" placeholder="Senha">
                      <?= isset($errorSenha) ? $errorSenha : '' ?>
                    </div>
                  </div>
                   
                   <div class="form-group row">
                    <label for="inputPassword4" class="col-sm-2 col-form-label">Confirma Senha</label>
                    <div class="col-sm-5">
                      <input type="password" name="confirmaSenha" value="<?= isset($usuario['confirmaSenha']) ? $usuario['confirmaSenha'] : '' ?>" class="form-control" id="inputPassword3" placeholder="Confirma Senha">
                      <?= isset($errorConfirmaSenha) ? $errorConfirmaSenha : '' ?>
                      
                    </div>
                  </div>
                  
            
                  <div class="form-group row">
                    <div class="col-sm-1" style="margin-left:230px">
                      <button type="submit" class="btn btn-primary">ALTERAR</button>
                    </div>
                  </div>
                </form>
                
        </div>
       </body>
</html>       