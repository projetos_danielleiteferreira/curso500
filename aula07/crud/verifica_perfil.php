<?php


if($_SESSION['perfil'] != 1){
    echo '<div class="alert alert-warning" role="alert">
            Acesso negado, você nao tem permissão para essa ação!
            <a href="javascript: history.go(-1)">  Voltar</a>
          </div>';
       exit();
}