<?php require 'verifica_login.php';?>

<html>
    <head>
    	<title>Listagem de Usuários</title>
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
		 <?php include 'menu.php'; ?>
		 <?php require 'verifica_perfil.php';?>
		 
        <div id="main">
			<table class="table">
			<h1>LISTA DE USUÁRIOS</h1>
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">NOME</th>
                  <th scope="col">EMAIL</th>
                  <th scope="col">SENHA</th>
                  <th scope="col">PERFIL</th>
                  <th scope="col">AÇÕES</th>
                  
                </tr>
              </thead>
              <tbody>
              	<?php 
                    //Inicio do Fluxo
                    require 'conexao.php';
                    
                    $itensPaginas = 10;
                    $totalRegistro = 0;
                    $pagina = $_GET['pagina'] ?? 1;
                    
                    $query = 'select count(*) as total from usuarios';
                    $result = pg_exec($query);
                    $retorno = pg_fetch_assoc($result);
                    
                    $totalRegistro = $retorno['total'];
                    
                    $totalPagina = ceil($totalRegistro/$itensPaginas);
                    $inicio = ($itensPaginas*$pagina) -$itensPaginas;
                    
                    $query = "select * from usuarios order by id offset $inicio limit $itensPaginas";
                    //$query = 'select * from usuarios order by id';
                    
                    //var_dump($query);
                    //exit();
                    $result = pg_query($query);
                    $usuarios = pg_fetch_all($result);
              	    foreach ($usuarios as $usuario):

              	?>
                <tr>
                  <th scope="row"><?= $usuario['id'];?></th>
                  <td><?= $usuario['nome']?></td>
                  <td><?= $usuario['email']?></td>
                  <td>*****</td>
                  <td><?= $usuario['perfil'] == 1 ? 'Administrador' : 'Secretaria'?></td>
                  <td>
                  	<a href="alterar_usuario.php?id=<?=$usuario['id']?>">Alterar</a> | 
                  	<a href="excluir_usuario.php?id=<?=$usuario['id']?>">Excluir</a>
                  </td>
                </tr>
                
                <?php 
                   endforeach;
                  
                ?>
                
              </tbody>
            </table>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <li class="page-item"><a class="page-link" href="?pagina=1">Inicio</a></li>
                
                <?php for($x=1; $x <= $totalPagina; $x ++): ?>
                        <li class="page-item"><a class="page-link" href="?pagina=<?=$x?>"><?=$x?></a></li>
                <?php endfor?>
                
                <li class="page-item"><a class="page-link" href="?pagina=<?= $totalPagina?>">Fim</a></li>
              </ul>
            </nav>
        </div>
    </body>
</html>