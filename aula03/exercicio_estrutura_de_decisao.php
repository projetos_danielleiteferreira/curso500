<?php

$aluno = '';
$bimestre1 = 5;
$bimestre2 = 7;
$bimestre3 = 8;
$bimestre4 = 9;
$faltas = 10;
$aulasDadas = 50;
/**
 * 1 - calcular a média aritimética da aluna
 * 2 - calcular a porcentagem de faltas
 * 3 - exibir o seguinte relatório: [
 *      - Nome do Aluno
 *      - Notas
 *      - Médias
 *      - Status
 *     O status é definido pelas seguintes condições:
 *          - média >= 7 e porcentagem de faltas <= 25% : Aprovado
 *          - média < 7 e porcentagem de faltas <= 25% : Reprovado
 *          - média < 7 e porcentagem de faltas <= 25% : Recuperação
 *          - média >= 7 e porcentam de faltas > 25% : Reprovado
 */

$somaNota = $bimestre1 + $bimestre2 + $bimestre3 + $bimestre4;
$mediaNota = $somaNota / 4;
$porcentagemFaltas = $faltas / $aulasDadas * 100;


if ($porcentagemFaltas > 25){
    $status = 'Reprovado';
}elseif ($mediaNota < 7){
    $status = 'Recuperação';
}else{
    $status = 'Aprovado';
}


echo "RELATÓRIO FINAL <br><hr>";
echo "Nome do aluno: " . ($aluno ? "$aluno<br>" : 'Aluno Sem Nome' . '<br>');
echo "<br>Média aritimética: <strong>$mediaNota</strong>";
echo "<br>$porcentagemFaltas% de faltas";
echo "<hr>";
echo $status;

