<?php

json
$aluno2 = [
    'nome' => 'Selma Sampaio',
    'turma' => '2B',
    'materia' => [
        'nome' => 'Matemática',
        'notas' => [
            'bimestre1' => 5,
            'bimestre2' => 7,
            'bimestre3' => 8,
            'bimestre4' => 8
        ]
    ]
];


/**
 * Escrever um algoritimo que imprime: 
 * Nome / Turma / Materia / Notas dos Bimestres / Média Final 
 */

$alunos = [$aluno1, $aluno2];

// foreach ($alunos as $chave => $aluno ) {
//     echo 'Nome: ' . $aluno['nome'] . '<br>';
//     echo 'Turma: ' . $aluno['turma'] . '<br>';
//     echo 'Materia: ' . $aluno['materia']['nome'] . '<br>';
//     echo 'Notas: ' . $aluno['materia']['notas']['bimestre1'];
//         echo ' | ' . $aluno['materia']['notas']['bimestre2'];
//         echo ' | ' . $aluno['materia']['notas']['bimestre3'];
//         echo ' | ' . $aluno['materia']['notas']['bimestre4'];

//    $media = $aluno['materia']['notas']['bimestre1'] + 
//                   ['materia']['notas']['bimestre2'] +
//                   ['materia']['notas']['bimestre3'] +
//                   ['materia']['notas']['bimestre4'] / 4;
        
//     echo '<br>Média Final: ' . $media; 
//     echo '<hr>';        
// }


foreach ($alunos as $aluno){
    echo 'Nome: ' . $aluno['nome'] . '<br>';
    echo 'Turma: ' . $aluno['turma'] . '<br>';
    echo 'Matéria: ' . ['materia']['nome'] . '<br>';
    
    $total = 0;
    $notas = '';
    
    foreach ($aluno['materia']['notas'] as $nota){
        $total += $nota;
        $notas .= "$nota |";
    }
    $media = $total / 4;
    echo 'Notas: ' . $notas . '<br>';
    echo 'Média: ' . $media;
    
    echo '<hr>';
    
}
